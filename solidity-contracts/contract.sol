pragma solidity >=0.4.21 <0.7.0;

contract TickdtProject {
   struct ticket{
       string idInDB;
       string hashedInfos;
   }
   event LogReturn(
   address MyAddress,
   string idInDB,
   string hashedInfos
   );
   mapping(address =>ticket[]) public properties;
   mapping(address =>bool) public clubs;
   function add(address _address,string memory _idInDB,string memory _hashedInfos)  public {
   ticket memory T;
   T.idInDB = _idInDB;
   T.hashedInfos = _hashedInfos;
   properties[_address].push(T);
   emit LogReturn(_address, _idInDB, _hashedInfos);
   }
   function accessCheck(address _senderAddress) public view returns(bool){
       if(clubs[_senderAddress]==true)
       return true ;
       else
       return false ;
   }
   function addAgent(address _address)  public {
   clubs[_address] = true;
   }
}